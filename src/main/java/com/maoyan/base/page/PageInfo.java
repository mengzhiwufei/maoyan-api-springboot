package com.maoyan.base.page;

import java.util.List;

/**
 * 分页实体
 * @author xieshi
 */
public class PageInfo {
	private int current; // 当前页
	private int perPage = 15; //每页条数
	private long size; // 总条数
	private List items;
	public int getCurrent() {
		return current;
	}
	public void setCurrent(int current) {
		this.current = current;
	}
	public int getPerPage() {
		return perPage;
	}
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public List getItems() {
		return items;
	}
	public void setItems(List items) {
		this.items = items;
	}

}

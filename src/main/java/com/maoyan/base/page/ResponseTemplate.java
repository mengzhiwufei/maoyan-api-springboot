package com.maoyan.base.page;
/**
 * 响应模板
 * @author xieshi
 *
 */
public class ResponseTemplate {
	private String status; //状态（SUCCESS--成功，FAILED--失败）
	private String errorCode; // 错误代码
	private String errorMsg; //错误信息
	private String errorStack; //错误堆栈信息
	private Object data; // 数据
	
	public ResponseTemplate(String status, String errorCode, String errorMsg, String errorStack, Object data) {
		super();
		this.status = status;
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		this.errorStack = errorStack;
		this.data = data;
	}
	
	public static ResponseTemplate success(Object data) {
		return new ResponseTemplate("SUCCESS",null,null,null,data);
	}
	
	public static ResponseTemplate failed(String errorCode, String errorMsg, String errorStack) {
		return new ResponseTemplate("FAILED", errorCode, errorMsg, errorStack, null);
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getErrorStack() {
		return errorStack;
	}
	public void setErrorStack(String errorStack) {
		this.errorStack = errorStack;
	}
}

package com.maoyan.base.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.maoyan.base.exception.BusinessException;
import com.maoyan.base.page.PageInfo;
import com.maoyan.base.util.ThreadLocalContext;
@Component
public class PageInterceptor implements HandlerInterceptor{

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		
	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse arg1, Object arg2) throws Exception {
		try {			
			String url = req.getRequestURI().toString();
			String pageStr = url.split("page/")[1];
			String currentStr = pageStr.split("/")[0];
			String pageSizeStr = pageStr.split("/")[1];
			PageInfo page = new PageInfo();
			page.setCurrent(Integer.parseInt(currentStr));
			int pageSize = Integer.parseInt(pageSizeStr);
			if(pageSize < 15) {
				page.setPerPage(pageSize);
			}
			ThreadLocalContext.set("page", page);
		}catch(Exception e) {
			throw new BusinessException("分页信息解析错误",e);
		}
		return true;
	}

}

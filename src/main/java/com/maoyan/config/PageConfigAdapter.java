package com.maoyan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.maoyan.base.interceptor.PageInterceptor;

@Configuration
public class PageConfigAdapter extends WebMvcConfigurerAdapter{
	@Autowired
	private PageInterceptor pageInterceptor;
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(pageInterceptor).addPathPatterns("/**/page/**");
	}
}

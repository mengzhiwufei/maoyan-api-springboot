package com.maoyan.movie.model;

public class Actor {
    private Integer actorId;

    private String actorName;

    private String actorHref;

    private String actorSrc;

    private String actorMask;

    private String movieId;

    public Integer getActorId() {
        return actorId;
    }

    public void setActorId(Integer actorId) {
        this.actorId = actorId;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName == null ? null : actorName.trim();
    }

    public String getActorHref() {
        return actorHref;
    }

    public void setActorHref(String actorHref) {
        this.actorHref = actorHref == null ? null : actorHref.trim();
    }

    public String getActorSrc() {
        return actorSrc;
    }

    public void setActorSrc(String actorSrc) {
        this.actorSrc = actorSrc == null ? null : actorSrc.trim();
    }

    public String getActorMask() {
        return actorMask;
    }

    public void setActorMask(String actorMask) {
        this.actorMask = actorMask == null ? null : actorMask.trim();
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId == null ? null : movieId.trim();
    }
}
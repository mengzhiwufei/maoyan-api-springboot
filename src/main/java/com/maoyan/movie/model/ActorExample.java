package com.maoyan.movie.model;

import java.util.ArrayList;
import java.util.List;

public class ActorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ActorExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andActorIdIsNull() {
            addCriterion("actor_id is null");
            return (Criteria) this;
        }

        public Criteria andActorIdIsNotNull() {
            addCriterion("actor_id is not null");
            return (Criteria) this;
        }

        public Criteria andActorIdEqualTo(Integer value) {
            addCriterion("actor_id =", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdNotEqualTo(Integer value) {
            addCriterion("actor_id <>", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdGreaterThan(Integer value) {
            addCriterion("actor_id >", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("actor_id >=", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdLessThan(Integer value) {
            addCriterion("actor_id <", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdLessThanOrEqualTo(Integer value) {
            addCriterion("actor_id <=", value, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdIn(List<Integer> values) {
            addCriterion("actor_id in", values, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdNotIn(List<Integer> values) {
            addCriterion("actor_id not in", values, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdBetween(Integer value1, Integer value2) {
            addCriterion("actor_id between", value1, value2, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorIdNotBetween(Integer value1, Integer value2) {
            addCriterion("actor_id not between", value1, value2, "actorId");
            return (Criteria) this;
        }

        public Criteria andActorNameIsNull() {
            addCriterion("actor_name is null");
            return (Criteria) this;
        }

        public Criteria andActorNameIsNotNull() {
            addCriterion("actor_name is not null");
            return (Criteria) this;
        }

        public Criteria andActorNameEqualTo(String value) {
            addCriterion("actor_name =", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameNotEqualTo(String value) {
            addCriterion("actor_name <>", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameGreaterThan(String value) {
            addCriterion("actor_name >", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameGreaterThanOrEqualTo(String value) {
            addCriterion("actor_name >=", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameLessThan(String value) {
            addCriterion("actor_name <", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameLessThanOrEqualTo(String value) {
            addCriterion("actor_name <=", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameLike(String value) {
            addCriterion("actor_name like", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameNotLike(String value) {
            addCriterion("actor_name not like", value, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameIn(List<String> values) {
            addCriterion("actor_name in", values, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameNotIn(List<String> values) {
            addCriterion("actor_name not in", values, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameBetween(String value1, String value2) {
            addCriterion("actor_name between", value1, value2, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorNameNotBetween(String value1, String value2) {
            addCriterion("actor_name not between", value1, value2, "actorName");
            return (Criteria) this;
        }

        public Criteria andActorHrefIsNull() {
            addCriterion("actor_href is null");
            return (Criteria) this;
        }

        public Criteria andActorHrefIsNotNull() {
            addCriterion("actor_href is not null");
            return (Criteria) this;
        }

        public Criteria andActorHrefEqualTo(String value) {
            addCriterion("actor_href =", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefNotEqualTo(String value) {
            addCriterion("actor_href <>", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefGreaterThan(String value) {
            addCriterion("actor_href >", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefGreaterThanOrEqualTo(String value) {
            addCriterion("actor_href >=", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefLessThan(String value) {
            addCriterion("actor_href <", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefLessThanOrEqualTo(String value) {
            addCriterion("actor_href <=", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefLike(String value) {
            addCriterion("actor_href like", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefNotLike(String value) {
            addCriterion("actor_href not like", value, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefIn(List<String> values) {
            addCriterion("actor_href in", values, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefNotIn(List<String> values) {
            addCriterion("actor_href not in", values, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefBetween(String value1, String value2) {
            addCriterion("actor_href between", value1, value2, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorHrefNotBetween(String value1, String value2) {
            addCriterion("actor_href not between", value1, value2, "actorHref");
            return (Criteria) this;
        }

        public Criteria andActorSrcIsNull() {
            addCriterion("actor_src is null");
            return (Criteria) this;
        }

        public Criteria andActorSrcIsNotNull() {
            addCriterion("actor_src is not null");
            return (Criteria) this;
        }

        public Criteria andActorSrcEqualTo(String value) {
            addCriterion("actor_src =", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcNotEqualTo(String value) {
            addCriterion("actor_src <>", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcGreaterThan(String value) {
            addCriterion("actor_src >", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcGreaterThanOrEqualTo(String value) {
            addCriterion("actor_src >=", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcLessThan(String value) {
            addCriterion("actor_src <", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcLessThanOrEqualTo(String value) {
            addCriterion("actor_src <=", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcLike(String value) {
            addCriterion("actor_src like", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcNotLike(String value) {
            addCriterion("actor_src not like", value, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcIn(List<String> values) {
            addCriterion("actor_src in", values, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcNotIn(List<String> values) {
            addCriterion("actor_src not in", values, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcBetween(String value1, String value2) {
            addCriterion("actor_src between", value1, value2, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorSrcNotBetween(String value1, String value2) {
            addCriterion("actor_src not between", value1, value2, "actorSrc");
            return (Criteria) this;
        }

        public Criteria andActorMaskIsNull() {
            addCriterion("actor_mask is null");
            return (Criteria) this;
        }

        public Criteria andActorMaskIsNotNull() {
            addCriterion("actor_mask is not null");
            return (Criteria) this;
        }

        public Criteria andActorMaskEqualTo(String value) {
            addCriterion("actor_mask =", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskNotEqualTo(String value) {
            addCriterion("actor_mask <>", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskGreaterThan(String value) {
            addCriterion("actor_mask >", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskGreaterThanOrEqualTo(String value) {
            addCriterion("actor_mask >=", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskLessThan(String value) {
            addCriterion("actor_mask <", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskLessThanOrEqualTo(String value) {
            addCriterion("actor_mask <=", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskLike(String value) {
            addCriterion("actor_mask like", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskNotLike(String value) {
            addCriterion("actor_mask not like", value, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskIn(List<String> values) {
            addCriterion("actor_mask in", values, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskNotIn(List<String> values) {
            addCriterion("actor_mask not in", values, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskBetween(String value1, String value2) {
            addCriterion("actor_mask between", value1, value2, "actorMask");
            return (Criteria) this;
        }

        public Criteria andActorMaskNotBetween(String value1, String value2) {
            addCriterion("actor_mask not between", value1, value2, "actorMask");
            return (Criteria) this;
        }

        public Criteria andMovieIdIsNull() {
            addCriterion("movie_id is null");
            return (Criteria) this;
        }

        public Criteria andMovieIdIsNotNull() {
            addCriterion("movie_id is not null");
            return (Criteria) this;
        }

        public Criteria andMovieIdEqualTo(String value) {
            addCriterion("movie_id =", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdNotEqualTo(String value) {
            addCriterion("movie_id <>", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdGreaterThan(String value) {
            addCriterion("movie_id >", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdGreaterThanOrEqualTo(String value) {
            addCriterion("movie_id >=", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdLessThan(String value) {
            addCriterion("movie_id <", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdLessThanOrEqualTo(String value) {
            addCriterion("movie_id <=", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdLike(String value) {
            addCriterion("movie_id like", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdNotLike(String value) {
            addCriterion("movie_id not like", value, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdIn(List<String> values) {
            addCriterion("movie_id in", values, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdNotIn(List<String> values) {
            addCriterion("movie_id not in", values, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdBetween(String value1, String value2) {
            addCriterion("movie_id between", value1, value2, "movieId");
            return (Criteria) this;
        }

        public Criteria andMovieIdNotBetween(String value1, String value2) {
            addCriterion("movie_id not between", value1, value2, "movieId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
package com.maoyan.movie.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maoyan.base.page.ResponseTemplate;
import com.maoyan.movie.model.Movie;
import com.maoyan.movie.service.MovieService;

@RestController
@RequestMapping("/movie")
public class MovieController {
	Logger logger = LoggerFactory.getLogger(MovieController.class);
	@Autowired
	private MovieService movieService;
	
	@RequestMapping(path="/onShow/page/{current}/{perPage}",method= RequestMethod.GET)
	public ResponseTemplate queryOnShow(@PathVariable int current,@PathVariable int perPage) {
		Movie movie = new Movie();
		movie.setStatus("0");
		return ResponseTemplate.success(movieService.queryForPage(movie));
	}
	@RequestMapping(path="/expect",method= RequestMethod.GET)
	public ResponseTemplate queryExpect() {
		Movie movie = new Movie();
		movie.setExpect("1");
		return ResponseTemplate.success(movieService.queryForPage(movie));
	}
	@RequestMapping(path="/notShow/page/{current}/{perPage}",method= RequestMethod.GET)
	public ResponseTemplate queryNotShow(@PathVariable int current,@PathVariable int perPage) {
		Movie movie = new Movie();
		movie.setStatus("2");
		return ResponseTemplate.success(movieService.queryForPage(movie));
	}
	@RequestMapping(path="/detail/{movieId}",method= RequestMethod.GET)
	public ResponseTemplate queryDetail(@PathVariable String movieId){
		return ResponseTemplate.success(movieService.queryDetail(movieId));
	}
}

package com.maoyan.movie.service;

import com.maoyan.base.page.PageInfo;
import com.maoyan.movie.model.Movie;

public interface MovieService {
	public int insert(Movie record);
	public PageInfo queryForPage(Movie movie);
	public Movie queryDetail(String movieId);
}

package com.maoyan.movie.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maoyan.movie.mapper.ActorMapper;
import com.maoyan.movie.model.Actor;
import com.maoyan.movie.service.ActorService;
@Service
public class ActorServiceImpl implements ActorService{
	@Autowired
	private ActorMapper actorMapper;
	public int insert(Actor record) {
		return actorMapper.insert(record);
	}
}

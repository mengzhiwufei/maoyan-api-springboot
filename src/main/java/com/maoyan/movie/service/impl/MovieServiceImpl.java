package com.maoyan.movie.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maoyan.base.page.PageInfo;
import com.maoyan.base.util.ThreadLocalContext;
import com.maoyan.movie.mapper.ActorMapper;
import com.maoyan.movie.mapper.MovieMapper;
import com.maoyan.movie.model.ActorExample;
import com.maoyan.movie.model.Movie;
import com.maoyan.movie.service.MovieService;
@Service
public class MovieServiceImpl implements MovieService{
	@Autowired
	private MovieMapper movieMapper;
	@Autowired
	private ActorMapper actorMapper;
	
	public int insert(Movie record) {
		return movieMapper.insert(record);
	}
	public PageInfo queryForPage(Movie movie){
		PageInfo page = (PageInfo)ThreadLocalContext.get("page");
		ThreadLocalContext.remove("page");
		long size = movieMapper.queryForPageCount(movie);
		List<Movie> list = new ArrayList<>();
		if(null != page) {
			list = movieMapper.queryForPage(movie,(page.getCurrent()-1) * page.getPerPage(), page.getPerPage());
		}else {
			page = new PageInfo();
			list = movieMapper.queryForPage(movie,null,null);
		}
		for(int i = 0; i < list.size(); i++) {
			Movie mo = list.get(i);
			ActorExample ae = new ActorExample();
			ae.createCriteria().andMovieIdEqualTo(mo.getMovieId());
			mo.setActors(actorMapper.selectByExample(ae));
		}
		page.setSize(size);
		page.setItems(list);
		return page;
	}
	public Movie queryDetail(String movieId){
		Movie movie = movieMapper.selectByPrimaryKey(movieId);
		ActorExample ae = new ActorExample();
		if(null != movie){			
			ae.createCriteria().andMovieIdEqualTo(movie.getMovieId());
			movie.setActors(actorMapper.selectByExample(ae));
		}
		return movie;
	}
}

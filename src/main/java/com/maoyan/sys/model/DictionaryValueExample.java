package com.maoyan.sys.model;

import java.util.ArrayList;
import java.util.List;

public class DictionaryValueExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DictionaryValueExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDicVidIsNull() {
            addCriterion("dic_vid is null");
            return (Criteria) this;
        }

        public Criteria andDicVidIsNotNull() {
            addCriterion("dic_vid is not null");
            return (Criteria) this;
        }

        public Criteria andDicVidEqualTo(Integer value) {
            addCriterion("dic_vid =", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidNotEqualTo(Integer value) {
            addCriterion("dic_vid <>", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidGreaterThan(Integer value) {
            addCriterion("dic_vid >", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidGreaterThanOrEqualTo(Integer value) {
            addCriterion("dic_vid >=", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidLessThan(Integer value) {
            addCriterion("dic_vid <", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidLessThanOrEqualTo(Integer value) {
            addCriterion("dic_vid <=", value, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidIn(List<Integer> values) {
            addCriterion("dic_vid in", values, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidNotIn(List<Integer> values) {
            addCriterion("dic_vid not in", values, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidBetween(Integer value1, Integer value2) {
            addCriterion("dic_vid between", value1, value2, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicVidNotBetween(Integer value1, Integer value2) {
            addCriterion("dic_vid not between", value1, value2, "dicVid");
            return (Criteria) this;
        }

        public Criteria andDicCodeIsNull() {
            addCriterion("dic_code is null");
            return (Criteria) this;
        }

        public Criteria andDicCodeIsNotNull() {
            addCriterion("dic_code is not null");
            return (Criteria) this;
        }

        public Criteria andDicCodeEqualTo(String value) {
            addCriterion("dic_code =", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeNotEqualTo(String value) {
            addCriterion("dic_code <>", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeGreaterThan(String value) {
            addCriterion("dic_code >", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeGreaterThanOrEqualTo(String value) {
            addCriterion("dic_code >=", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeLessThan(String value) {
            addCriterion("dic_code <", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeLessThanOrEqualTo(String value) {
            addCriterion("dic_code <=", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeLike(String value) {
            addCriterion("dic_code like", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeNotLike(String value) {
            addCriterion("dic_code not like", value, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeIn(List<String> values) {
            addCriterion("dic_code in", values, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeNotIn(List<String> values) {
            addCriterion("dic_code not in", values, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeBetween(String value1, String value2) {
            addCriterion("dic_code between", value1, value2, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicCodeNotBetween(String value1, String value2) {
            addCriterion("dic_code not between", value1, value2, "dicCode");
            return (Criteria) this;
        }

        public Criteria andDicVnameIsNull() {
            addCriterion("dic_vname is null");
            return (Criteria) this;
        }

        public Criteria andDicVnameIsNotNull() {
            addCriterion("dic_vname is not null");
            return (Criteria) this;
        }

        public Criteria andDicVnameEqualTo(String value) {
            addCriterion("dic_vname =", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameNotEqualTo(String value) {
            addCriterion("dic_vname <>", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameGreaterThan(String value) {
            addCriterion("dic_vname >", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameGreaterThanOrEqualTo(String value) {
            addCriterion("dic_vname >=", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameLessThan(String value) {
            addCriterion("dic_vname <", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameLessThanOrEqualTo(String value) {
            addCriterion("dic_vname <=", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameLike(String value) {
            addCriterion("dic_vname like", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameNotLike(String value) {
            addCriterion("dic_vname not like", value, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameIn(List<String> values) {
            addCriterion("dic_vname in", values, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameNotIn(List<String> values) {
            addCriterion("dic_vname not in", values, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameBetween(String value1, String value2) {
            addCriterion("dic_vname between", value1, value2, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVnameNotBetween(String value1, String value2) {
            addCriterion("dic_vname not between", value1, value2, "dicVname");
            return (Criteria) this;
        }

        public Criteria andDicVcodeIsNull() {
            addCriterion("dic_vcode is null");
            return (Criteria) this;
        }

        public Criteria andDicVcodeIsNotNull() {
            addCriterion("dic_vcode is not null");
            return (Criteria) this;
        }

        public Criteria andDicVcodeEqualTo(String value) {
            addCriterion("dic_vcode =", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeNotEqualTo(String value) {
            addCriterion("dic_vcode <>", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeGreaterThan(String value) {
            addCriterion("dic_vcode >", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeGreaterThanOrEqualTo(String value) {
            addCriterion("dic_vcode >=", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeLessThan(String value) {
            addCriterion("dic_vcode <", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeLessThanOrEqualTo(String value) {
            addCriterion("dic_vcode <=", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeLike(String value) {
            addCriterion("dic_vcode like", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeNotLike(String value) {
            addCriterion("dic_vcode not like", value, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeIn(List<String> values) {
            addCriterion("dic_vcode in", values, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeNotIn(List<String> values) {
            addCriterion("dic_vcode not in", values, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeBetween(String value1, String value2) {
            addCriterion("dic_vcode between", value1, value2, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andDicVcodeNotBetween(String value1, String value2) {
            addCriterion("dic_vcode not between", value1, value2, "dicVcode");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
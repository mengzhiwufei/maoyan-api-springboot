package com.maoyan.sys.model;

public class DictionaryValue {
    private Integer dicVid;

    private String dicCode;

    private String dicVname;

    private String dicVcode;

    private Integer sort;

    public Integer getDicVid() {
        return dicVid;
    }

    public void setDicVid(Integer dicVid) {
        this.dicVid = dicVid;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode == null ? null : dicCode.trim();
    }

    public String getDicVname() {
        return dicVname;
    }

    public void setDicVname(String dicVname) {
        this.dicVname = dicVname == null ? null : dicVname.trim();
    }

    public String getDicVcode() {
        return dicVcode;
    }

    public void setDicVcode(String dicVcode) {
        this.dicVcode = dicVcode == null ? null : dicVcode.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
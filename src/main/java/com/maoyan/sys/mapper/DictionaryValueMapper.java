package com.maoyan.sys.mapper;

import com.maoyan.sys.model.DictionaryValue;
import com.maoyan.sys.model.DictionaryValueExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DictionaryValueMapper {
    long countByExample(DictionaryValueExample example);

    int deleteByExample(DictionaryValueExample example);

    int deleteByPrimaryKey(Integer dicVid);

    int insert(DictionaryValue record);

    int insertSelective(DictionaryValue record);

    List<DictionaryValue> selectByExample(DictionaryValueExample example);

    DictionaryValue selectByPrimaryKey(Integer dicVid);

    int updateByExampleSelective(@Param("record") DictionaryValue record, @Param("example") DictionaryValueExample example);

    int updateByExample(@Param("record") DictionaryValue record, @Param("example") DictionaryValueExample example);

    int updateByPrimaryKeySelective(DictionaryValue record);

    int updateByPrimaryKey(DictionaryValue record);
}
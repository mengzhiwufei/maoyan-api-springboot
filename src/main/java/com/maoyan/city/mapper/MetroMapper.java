package com.maoyan.city.mapper;

import com.maoyan.city.model.Metro;
import com.maoyan.city.model.MetroExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MetroMapper {
    long countByExample(MetroExample example);

    int deleteByExample(MetroExample example);

    int deleteByPrimaryKey(Integer metroId);

    int insert(Metro record);

    int insertSelective(Metro record);

    List<Metro> selectByExample(MetroExample example);

    Metro selectByPrimaryKey(Integer metroId);

    int updateByExampleSelective(@Param("record") Metro record, @Param("example") MetroExample example);

    int updateByExample(@Param("record") Metro record, @Param("example") MetroExample example);

    int updateByPrimaryKeySelective(Metro record);

    int updateByPrimaryKey(Metro record);
}
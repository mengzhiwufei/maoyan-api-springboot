package com.maoyan.city.mapper;

import com.maoyan.city.model.BusinessDistrict;
import com.maoyan.city.model.BusinessDistrictExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BusinessDistrictMapper {
    long countByExample(BusinessDistrictExample example);

    int deleteByExample(BusinessDistrictExample example);

    int deleteByPrimaryKey(Integer districtId);

    int insert(BusinessDistrict record);

    int insertSelective(BusinessDistrict record);

    List<BusinessDistrict> selectByExample(BusinessDistrictExample example);

    BusinessDistrict selectByPrimaryKey(Integer districtId);

    int updateByExampleSelective(@Param("record") BusinessDistrict record, @Param("example") BusinessDistrictExample example);

    int updateByExample(@Param("record") BusinessDistrict record, @Param("example") BusinessDistrictExample example);

    int updateByPrimaryKeySelective(BusinessDistrict record);

    int updateByPrimaryKey(BusinessDistrict record);
}
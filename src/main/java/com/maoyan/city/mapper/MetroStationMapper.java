package com.maoyan.city.mapper;

import com.maoyan.city.model.MetroStation;
import com.maoyan.city.model.MetroStationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MetroStationMapper {
    long countByExample(MetroStationExample example);

    int deleteByExample(MetroStationExample example);

    int deleteByPrimaryKey(Integer stationId);

    int insert(MetroStation record);

    int insertSelective(MetroStation record);

    List<MetroStation> selectByExample(MetroStationExample example);

    MetroStation selectByPrimaryKey(Integer stationId);

    int updateByExampleSelective(@Param("record") MetroStation record, @Param("example") MetroStationExample example);

    int updateByExample(@Param("record") MetroStation record, @Param("example") MetroStationExample example);

    int updateByPrimaryKeySelective(MetroStation record);

    int updateByPrimaryKey(MetroStation record);
}
package com.maoyan.city.service;

import java.util.List;
import java.util.Map;

import com.maoyan.city.model.City;

public interface CityService {
	public Map<String, List<City>> queryCities() throws Exception;
	public void insert(City city);
}

package com.maoyan.city.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maoyan.city.mapper.CityMapper;
import com.maoyan.city.model.City;
import com.maoyan.city.model.CityExample;
import com.maoyan.city.service.CityService;

@Service
public class CityServiceImpl implements CityService{
	@Autowired
	private CityMapper cityMapper;
	
	public Map<String, List<City>> queryCities() throws Exception{
		List<City> allCities = cityMapper.selectByExample(null);
		CityExample example = new CityExample();
		CityExample.Criteria criteria= example.createCriteria();
		criteria.andHotEqualTo("1");
		List<City> hotCities = cityMapper.selectByExample(example);
		Map<String, List<City>> map = new HashMap<String, List<City>>();
		map.put("all", allCities);
		map.put("hot", hotCities);
		return map;
	}
	public void insert(City city) {
		cityMapper.insert(city);
	}
}

package com.maoyan.city.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maoyan.city.model.City;
import com.maoyan.city.service.CityService;

@RestController
@RequestMapping("/city")
public class CityController {
	Logger logger = LoggerFactory.getLogger(CityController.class);
	@Autowired
	private CityService cityService;

	@RequestMapping(path="/all",method= RequestMethod.GET)
	public Map<String, List<City>> queryCities(){
		Map<String, List<City>> map = null;
		try {
			return cityService.queryCities();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

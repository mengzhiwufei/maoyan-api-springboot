package com.maoyan.city.model;

public class Cinema {
    private Integer id;

    private String address;

    private String href;

    private String name;

    private String lon;

    private String lat;

    private String tags;

    private String brand;

    private String businessdistrict;

    private String metrostation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href == null ? null : href.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon == null ? null : lon.trim();
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat == null ? null : lat.trim();
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags == null ? null : tags.trim();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

    public String getBusinessdistrict() {
        return businessdistrict;
    }

    public void setBusinessdistrict(String businessdistrict) {
        this.businessdistrict = businessdistrict == null ? null : businessdistrict.trim();
    }

    public String getMetrostation() {
        return metrostation;
    }

    public void setMetrostation(String metrostation) {
        this.metrostation = metrostation == null ? null : metrostation.trim();
    }
}
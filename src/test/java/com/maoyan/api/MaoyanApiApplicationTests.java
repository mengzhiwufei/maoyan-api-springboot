package com.maoyan.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.maoyan.city.service.CityService;
import com.maoyan.movie.model.Actor;
import com.maoyan.movie.model.Movie;
import com.maoyan.movie.service.ActorService;
import com.maoyan.movie.service.MovieService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MaoyanApiApplicationTests {
	@Autowired
	private CityService cityService;
	@Autowired
	private MovieService movieService;
	@Autowired
	private ActorService actorService;
	@Test
	public void contextLoads() {
		try {
//			cityService.queryCities().size();
			read();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void read() throws IOException {
		File file = new File("/Users/xieshi/Documents/Laboratory/gitee/maoyan-api-springboot/src/test/java/movies.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder result = new StringBuilder();
		String s = null;
        while((s = br.readLine())!=null){//使用readLine方法，一次读一行
            result.append(System.lineSeparator()+s);
        }
        br.close(); 
        analysis(result.toString());
//        JSONArray jsonArray = (JSONArray) JSON.parse(json);
//        for(int i=0;i<jsonArray.size();i++) {
//        		JSONObject obj = (JSONObject) jsonArray.get(i);
//        		City city = new City();
//        		city.setCname(obj.get("cname")+"");
//        		city.setHot(obj.get("hot")+"");
//        		city.setPinyin(obj.get("pinyin")+"");
//        		city.setInitials(obj.get("initials")+"");
//        		cityService.insert(city);
//        }
	}
	public void analysis(String json) {
		List<MovieView> list = (List<MovieView>) JSON.parseArray(json,MovieView.class);
		try {
			for(MovieView mov :list) {
				String movieId = UUID.randomUUID().toString().replaceAll("-", "");
				Movie movie = new Movie();
				movie.setMovieId(movieId);
				movie.setCategoryCat(mov.getCategoryCat());
				movie.setCname(mov.getCname());
				movie.setCountry(mov.getCountry());
				movie.setDescb(mov.getDesc());
				movie.setDuration(mov.getDuration());
				movie.setEname(mov.getEname());
				movie.setExpect(mov.getExpect());
				movie.setImgSrc(mov.getImgSrc());
				String pnum = mov.getPeopleNum();
				if(pnum.indexOf("万")>-1) {
					pnum = Double.parseDouble(pnum.split("万")[0])*10000+"";
				}
				movie.setPeopleNum((int) Double.parseDouble(pnum));
				String releaseDate = mov.getReleaseDate();
				movie.setReleaseDate(releaseDate);
				movie.setScore(mov.getScore());
				movie.setStatus(mov.getStatus());
				movie.setWannerNum(mov.getWannerNum());
				movieService.insert(movie);
				System.out.println(JSON.toJSONString(movie));
				List<Actor> actors = mov.getActors();
				if(null != actors && actors.size()>0) {					
					for(Actor actor : actors) {
						actor.setMovieId(movieId);
						actorService.insert(actor);
					}
				}
			}	
		}catch(Exception e) {
			
		}
	}
}
